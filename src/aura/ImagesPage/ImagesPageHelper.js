({
    setupDataTable: function (component) {
        component.set('v.allImagesColumns', [
            {label: 'Album Id', fieldName: 'AlbumId__c', type: 'text', cellAttributes: { alignment: 'center' }},
            {label: 'Id', fieldName: 'Id__c', type: 'text', cellAttributes: { alignment: 'center' }},
            {label: 'Title', fieldName: 'Title__c',  type: 'text', wrapText: true, cellAttributes: { alignment: 'center' }},
            {label: 'URL', fieldName: 'Url__c',  type: 'url', cellAttributes: { alignment: 'center' }},
            {label: 'Thumbnail URL', fieldName: 'ThumbnailUrl__c',  type: 'url', cellAttributes: { alignment: 'center' }}
        ]);
        component.set('v.selectedImagesColumns', [
            {type: "button", initialWidth: 90, typeAttributes: {label: 'Delete', name: 'Delete', title: 'Delete', disabled: false, value: 'delete'}},
            {label: 'Album Id', fieldName: 'AlbumId__c', type: 'text', cellAttributes: { alignment: 'center' }},
            {label: 'Id', fieldName: 'Id__c', type: 'text', cellAttributes: { alignment: 'center' }},
            {label: 'Title', fieldName: 'Title__c',  type: 'text', wrapText: true, cellAttributes: { alignment: 'center' }},
            {label: 'URL', fieldName: 'Url__c',  type: 'url', cellAttributes: { alignment: 'center' }},
            {label: 'Thumbnail URL', fieldName: 'ThumbnailUrl__c',  type: 'url', cellAttributes: { alignment: 'center' }}
        ]);
    },

    fetchImages : function (component, event, offSetCount) {
        if (offSetCount <= 2000) {
            event.getSource().set("v.isLoading", true);
        
            var action = component.get("c.getImages");
            action.setParams({
                "offSet" : offSetCount
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var records = response.getReturnValue();
                    var currentData = component.get('v.imageList').concat(records);
                    component.set("v.imageList", currentData);
                    component.set("v.filteredImages", currentData);
                } else {
                    var errors = response.getError();
                    if (errors && errors.length > 0) {
                        this.showToastMessage(errors[0].message, "error");
                    }
                }
                event.getSource().set("v.isLoading", false);
            });
            $A.enqueueAction(action);
        } else {
            component.set("v.enableInfiniteLoading", false);
        }
    },

    handleSelectedRow: function (component, event) {
        component.set("v.currentSelectedRows", event.getParam('selectedRows'));
    },
    
    restoreAllImages: function (component) {
        if ($A.util.isEmpty(component.get("v.searchPhrase"))) {
            component.set("v.filteredImages", component.get("v.imageList"));
            component.set("v.enableInfiniteLoading", true);
        }
    },    
    
    collectSelectedRows: function (component) {
        let selectedRows = component.get("v.selectedImages");
        selectedRows = [...new Set(selectedRows.concat(component.get("v.currentSelectedRows")))];
        component.set("v.selectedImages", selectedRows);
        component.set("v.selectedImagesSize", selectedRows.length);
        component.set("v.selectedRows", []);
        component.set("v.currentSelectedRows", []);
    },

    searchImageByTitle : function (component) {
        let searchPhrase = component.get("v.searchPhrase");

        if (!$A.util.isEmpty(searchPhrase)) {
            component.set("v.enableInfiniteLoading", false);
            component.set("v.isLoading", true);
    
            var action = component.get("c.searchImages");
            action.setParams({
                "searchPhrase": searchPhrase
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.filteredImages", response.getReturnValue());
                } else {
                    var errors = response.getError();
                    if (errors && errors.length > 0) {
                        this.showToastMessage(errors[0].message, "error");
                    }
                }
                component.set("v.isLoading", false);
            });
            $A.enqueueAction(action);
        } else {
            this.showToastMessage("Please enter search phrase", "error");
        }
    },

    removeImageFromList: function (component, event) {
        var row = event.getParam('row');
        var rows = component.get('v.selectedImages');
        var rowIndex = rows.indexOf(row);
        rows.splice(rowIndex, 1);
        component.set('v.selectedImages', rows);
        component.set("v.selectedImagesSize", rows.length);
    },    
    
    sendImages: function (component) {
        let emailAddress = component.get("v.emailAddress");
        let selectedImages = component.get('v.selectedImages');

        if ($A.util.isEmpty(emailAddress)) {
            this.showToastMessage("Please enter email address", "error");
        } else if ($A.util.isEmpty(selectedImages)) {
            this.showToastMessage("Please select any image", "error");
        } else {
            component.set("v.isLoading", true);
    
            var action = component.get("c.sendImageList");
            action.setParams({
                "selectedImages": selectedImages,
                "emailAddress": emailAddress
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.selectedImages", []);
                    component.set("v.selectedImagesSize", 0);
                    component.set("v.emailAddress", "");

                    this.showToastMessage("Images has beed sent successfully", "success");
                } else {
                    var errors = response.getError();
                    if (errors && errors.length > 0) {
                        this.showToastMessage(errors[0].message, "error");
                    }
                }
                component.set("v.isLoading", false);
            });
            $A.enqueueAction(action);
        }
    },

    showToastMessage: function (message, type) {
        $A.get("e.force:showToast")
            .setParams({
                message: message,
                type: type
            })
            .fire();
    }
})