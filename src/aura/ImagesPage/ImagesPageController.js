({
    doInit: function(component, event, helper) {
        helper.setupDataTable(component);
        helper.fetchImages(component, event, 0);
        
    },

    loadMoreData: function(component, event, helper) {       
        helper.fetchImages(component, event, component.get('v.imageList').length);
    },

    handleSelectedRow: function (component, event, helper) {
        helper.handleSelectedRow(component, event);
    },

    onChangeSearchImageByTitle: function (component, event, helper) {
        helper.restoreAllImages(component);
    },
  
    searchImageByTitle: function (component, event, helper) {
        helper.collectSelectedRows(component);
        helper.searchImageByTitle(component);
    },    
    
    sendImagesViaEmail: function (component, event, helper) {
        helper.collectSelectedRows(component);
    },
    
    addImagesToSendList: function (component, event, helper) {
        helper.collectSelectedRows(component);
    },

    removeImageFromList : function(component, event, helper) {
        helper.removeImageFromList(component, event);
    },    
    
    sendImages : function(component, event, helper) {
        helper.sendImages(component);
    },
})