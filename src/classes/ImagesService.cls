/**
 * @author Pawel Burzak
 * @description Service class used to perform actions on Image__c records
 * @since 21.08.2020
*/
public with sharing class ImagesService {

    /**
     * @description Constructor of the class
     * @param none
     * @return none
    */
    public ImagesService() {}

    /**
     * @description Method used to get Images records
     * @param offSet
     * @return List<Image__c>
    */
    public List<Image__c> getImages(Integer offSet) {
        try {
            return [
                SELECT AlbumId__c, Id__c, ThumbnailUrl__c, Title__c, Url__c
                FROM Image__c
                LIMIT 25
                OFFSET :Integer.valueOf(offSet)
            ];
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * @description Method used to search Images records by title
     * @param searchPhrase
     * @return List<Image__c>
    */
    public List<Image__c> searchImages(String searchPhrase) {
        try {
            String phrase = '%' + String.escapeSingleQuotes(searchPhrase) + '%';
            return [
                SELECT AlbumId__c, Id__c, ThumbnailUrl__c, Title__c, Url__c
                FROM Image__c
                WHERE Title__c LIKE :phrase
            ];
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * @description Method used to send Images via Email
     * @param selectedImages
     * @param emailAddress
     * @return none
    */
    public void sendImageList(List<Image__c> selectedImages, String emailAddress) {
        try {
            Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();

            String body = 'List of Images:\n\n';
            for (Integer i = 0; i < selectedImages.size(); i++) {
                Image__c img = selectedImages.get(i);
                Integer imageNumber = i + 1;
                body += imageNumber + '. Id: ' + img.Id__c  + ' , Album Id: ' + img.AlbumId__c + ' , Title: ' + img.Title__c + 
                    ' , URL: ' + img.Url__c + ' , Thumbnail URL: ' + img.ThumbnailUrl__c + '\n';
            }

            emailMessage.setSenderDisplayName('Salesforce system notification');
            emailMessage.setSubject('Images List');
            emailMessage.setPlainTextBody(body);
            emailMessage.setToAddresses(new List<String>{emailAddress});

            List<Messaging.SendEmailResult> sendEmailResults = Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{emailMessage});

            if (System.Test.isRunningTest() && sendEmailResults.get(0).isSuccess()) {
                insert new Task(
                        OwnerId = UserInfo.getUserId(),
                        Subject = 'Emails with Image records were send successfully');
            }
        } catch (Exception ex) {
            throw ex;
        }
    }
}