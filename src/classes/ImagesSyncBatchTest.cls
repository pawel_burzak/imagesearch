/**
 * @author Pawel Burzak
 * @description Test class for ImagesSyncBatch
 * @since 20.08.2020
*/

@IsTest
private class ImagesSyncBatchTest {

    @IsTest
    static void shouldInsertImageRecordsFromExternalSystem() {

        System.Test.setMock(HttpCalloutMock.class, new ImageCalloutClient.ImageRecordsCalloutMock());
        
        System.Test.startTest();

        Database.executeBatch(new ImagesSyncBatch());

        System.Test.stopTest();

        List<Image__c> imageRecords = [
            SELECT Id
            FROM Image__c
        ];

        System.assertEquals(200, imageRecords.size());
    }
}