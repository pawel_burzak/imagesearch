/**
 * @author Pawel Burzak
 * @description Test class for ImagesSyncScheduler
 * @since 20.08.2020
*/

@IsTest
private class ImagesSyncSchedulerTest {

    @IsTest
    static void imagesSyncSchedulerTest() {
        System.Test.setMock(HttpCalloutMock.class, new ImageCalloutClient.ImageRecordsCalloutMock());

        String cron = '0 0 23 * * ? *';

        System.Test.startTest();
            String jobId = System.schedule('Test ImagesSyncScheduler',
                cron, new ImagesSyncScheduler());
                System.Test.stopTest();

        CronTrigger ct = [
            SELECT Id, CronExpression
            FROM CronTrigger
            WHERE Id = :jobId ];

        System.assertEquals(cron, ct.CronExpression);
    }
}