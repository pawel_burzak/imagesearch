/**
 * @author Pawel Burzak
 * @description Test class for ImagesController
 * @since 21.08.2020
*/

@IsTest
private class ImagesControllerTest {

    @TestSetup
    static void setupData() {
        List<Image__c> imageList = new List<Image__c>();
        Image__c img;

        for (Integer i = 0; i < 50; i++) {
            img = new Image__c();
            img.AlbumId__c = 1;
            img.Id__c = i;
            img.Title__c = 'Title ' + i;
            img.Url__c = 'Test Url';
            img.ThumbnailUrl__c = 'Test Thumbnail Url';

            imageList.add(img);
        }

        insert imageList;
    }

    @IsTest
    static void getImagesWithSuccess() {
        List<Image__c> result = ImagesController.getImages(0);

        System.assertEquals(25, result.size());
    }

    @IsTest
    static void getImagesWithError() {
        Boolean hasException = false;

        try {
            ImagesController.getImages(null);
        } catch (Exception ex) {
            hasException = true;
        }

        System.assertEquals(true, hasException);
    }

    @IsTest
    static void searchImagesWithSuccess() {
        List<Image__c> result = ImagesController.searchImages('Title');

        System.assertEquals(50, result.size());
    }

    @IsTest
    static void searchImagesWithError() {
        Boolean hasException = false;

        try {
            ImagesController.searchImages(null);
        } catch (Exception ex) {
            hasException = true;
        }

        System.assertEquals(true, hasException);
    }

    @IsTest
    static void sendImageListWithSuccess() {
        List<Image__c> imageList = [
            SELECT AlbumId__c, Id__c, Title__c, Url__c, ThumbnailUrl__c
            FROM Image__c
            LIMIT 5
        ];

        ImagesController.sendImageList(imageList, 'test@gmail.com');

        List<Task> tasksAfterSendingEmail = [
                SELECT Id
                FROM Task
                WHERE Subject = 'Emails with Image records were send successfully'
        ];

        System.assertEquals(1, tasksAfterSendingEmail.size());
    }

    @IsTest
    static void sendImageListWithError() {
        Boolean hasException = false;

        try {
            ImagesController.sendImageList(null, null);
        } catch (Exception ex) {
            hasException = true;
        }

        System.assertEquals(true, hasException);
    }
}