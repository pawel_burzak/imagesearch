/**
 * @author Pawel Burzak
 * @description Scheduler class to synchronize Image records in Batch
 * @since 20.08.2020
*/

global with sharing class ImagesSyncScheduler implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new ImagesSyncBatch());
    }
}