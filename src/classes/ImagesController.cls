/**
 * @author Pawel Burzak
 * @description Server side controller for ImagesPage component
 * @since 21.08.2020
*/

public with sharing class ImagesController {

    /**
     * @description Method used to get Images records
     * @param offSet
     * @return List<Image__c>
    */
    @AuraEnabled
    public static List<Image__c> getImages(Integer offSet) {
        try {
            return new ImagesService().getImages(offSet);
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }

    /**
     * @description Method used to search Images records by title
     * @param searchPhrase
     * @return List<Image__c>
    */
    @AuraEnabled
    public static List<Image__c> searchImages(String searchPhrase) {
        try {
            return new ImagesService().searchImages(searchPhrase);
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }

    /**
     * @description Method used to send Images via Email
     * @param selectedImages
     * @param emailAddress
     * @return none
    */
    @AuraEnabled
    public static void sendImageList(List<Image__c> selectedImages, String emailAddress) {
        try {
            new ImagesService().sendImageList(selectedImages, emailAddress);
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }
}