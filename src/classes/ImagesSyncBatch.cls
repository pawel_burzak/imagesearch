/**
 * @author Pawel Burzak
 * @description Batch class used to synchronize Image records from external system
 * @since 20.08.2020
*/

public with sharing class ImagesSyncBatch implements Database.Batchable<ImageCalloutClient.ImageResponse>, Database.AllowsCallouts {

    public List<ImageCalloutClient.ImageResponse> start(Database.BatchableContext bc) {
        return new ImageCalloutClient().getImages();
    }

    public void execute(Database.BatchableContext BC, List<ImageCalloutClient.ImageResponse> images) {
        try {
            List<Image__c> imagesToUpsert = new List<Image__c>();

            for(ImageCalloutClient.ImageResponse imageResponse : images) {
                imagesToUpsert.add(new Image__c(
                    AlbumId__c = imageResponse.albumId,
                    Id__c = imageResponse.id,
                    Title__c = imageResponse.title,
                    Url__c = imageResponse.url,
                    ThumbnailUrl__c = imageResponse.thumbnailUrl
                ));
            }

            upsert imagesToUpsert;
        } catch (Exception ex) {
            throw new SynchronizationException('Error occured during Images upsert.');
        }
    }

    public void finish(Database.BatchableContext BC) {
        delete [
            SELECT Id
            FROM Image__c
            WHERE LastModifiedDate < TODAY
        ];
    }

    /**
     * @description Custom exception class
    */
    public class SynchronizationException extends Exception {}
}
