/**
 * @author Pawel Burzak
 * @description Callout client class used to get Image records from external system
 * @since 20.08.2020
*/

public with sharing class ImageCalloutClient extends CalloutClient {
    private static final String ENDPOINT = 'callout:ImagesEndpoint';

    private ImageResponse imageResponse;
    
    /**
     * @description Constructor of the class
     * @param none
     * @return none
    */
    public ImageCalloutClient() {}


    /**
     * @description Method used to prepare request, do callout and handle response
     * @param none
     * @return List<ImageResponse>
    */
    public List<ImageResponse> getImages() {
        createRequest();
        doCallout();
        return handleResponse();
    }

    /**
     * @description Overridden method used to deserialize HTTP request body into ImageResponse object
     * @param none
     * @return object
    */
    public override Object getResponseObject() {
        try {
            return JSON.deserialize(getResponseBody(), List<ImageResponse>.class);
        } catch (JSONException ex) {
            throw new JSONException('Error occured during callout response deserialization');
        }
    }

    private void createRequest() {
        request = super.createRequest(HTTP_METHOD_GET);
        request.setEndpoint(ENDPOINT);
    }

    private List<ImageResponse> handleResponse() {
        if (isStatusCodeOk()) {
            return (List<ImageResponse>) getResponseObject();
        } else {
            throw new CalloutClientException('Invalid HTTP status: ' + getResponseStatusCode());
        }
    }

    private static String prepareImageRecordsResponse() {
        List<ImageResponse> imageResponseList = new List<ImageResponse>();
        ImageResponse imageResponse;

        for (Integer i = 0; i < 200; i++) {
            imageResponse = new ImageResponse();
            imageResponse.albumId = 1;
            imageResponse.id = i;
            imageResponse.title = 'Title ' + i;
            imageResponse.url = 'Test Url';
            imageResponse.thumbnailUrl = 'Test Thumbnail Url';

            imageResponseList.add(imageResponse);
        }

        return JSON.serialize(imageResponseList);
    }

    /**
     * @description Inner class to mock callout
    */
    public class ImageRecordsCalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            res.setBody(prepareImageRecordsResponse());

            return res;
        }
    }

    /**
     * @description Inner class for Response Wrapper
    */
    public class ImageResponse {
        public Integer albumId { get; set; }
        public Integer id { get; set; }
        public String title { get; set; }
        public String url { get; set; }
        public String thumbnailUrl { get; set; }
    }
}