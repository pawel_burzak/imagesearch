/**
 * @author Pawel Burzak
 * @description Abstract class that handling the HTTP methods
 * @since 20.08.2020
*/

public abstract with sharing class CalloutClient {
    protected final Integer DEFAULT_TIMEOUT = 120000;
    protected final String HEADER_CONTENT_TYPE = 'Content-Type';
    protected final String HEADER_CONTENT_TYPE_APPLICATION_JSON = 'application/json';
    protected final String HTTP_METHOD_GET = 'GET';

    protected HttpRequest request;
    protected HttpResponse response;

    /**
     * @description Method used to do a callout
     * @param none
     * @return none
    */
    protected void doCallout() {
        this.response = new Http().send(request);
    }

    /**
     * @description Method used to check if HTTP response status code is 200
     * @param none
     * @return Boolean
    */
    protected Boolean isStatusCodeOk() {
        return response.getStatusCode() == 200;
    }

    /**
     * @description Get HTTP response status code
     * @param none
     * @return Integer
    */
    protected Integer getResponseStatusCode() {
        return response.getStatusCode();
    }

    /**
     * @description Method used to get HTTP response body
     * @param none
     * @return String
    */
    protected String getResponseBody() {
        return response.getBody();
    }

    /**
     * @description Method used to prepare HTTP request
     * @param method - HTTP method
     * @return HTTPRequest
    */
    protected virtual HttpRequest createRequest(String method) {
        request = new HttpRequest();
        request.setMethod(method);
        request.setTimeout(DEFAULT_TIMEOUT);
        request.setHeader(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_APPLICATION_JSON);
        return request;
    }

    /**
     * @description Abstract method used to get response object
     * @param none
     * @return Object
    */
    protected abstract Object getResponseObject();

    /**
     * @description Custom exception class
    */
    public class CalloutClientException extends Exception {}
}